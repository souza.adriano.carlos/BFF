#Build
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

RUN mkdir /app
COPY . /app
RUN cd /app && ls -l
RUN mkdir /build

#Artefact
RUN cd /app && dotnet publish "bff-braspress.csproj" --self-contained true -r linux-x64 -o /build

#Final image
FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS final
WORKDIR /build
COPY --from=build /build .
#RUN mkdir /https
#COPY .certificates /https
RUN mkdir /var/log/bff
#RUN cd /https && ls -l
RUN cd /build && ls -l

##Copy Certificado Braspress.com.br
#COPY .certificates/braspress.com.br.crt /usr/local/share/ca-certificates/braspress.com.br.crt
#RUN update-ca-certificates

#CMD tail -f /dev/null 
ENTRYPOINT ["./bff-braspress"]
