using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yarp.ReverseProxy.Configuration;

namespace bff_braspress
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var policyName = "bypassCORS";

            services.AddCors(options =>
            {
                options.AddPolicy(policyName, b =>
                {
                    b.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "bff_braspress", Version = "v1" });
            });
            services.AddReverseProxy().LoadFromConfig(Configuration.GetSection("Yarp"));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("bypassCORS");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                var serviceProvider = app.ApplicationServices;
                app.UseSwagger();
                //app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "bff_braspress v1"));
                app.UseSwaggerUI(options =>
                {
                    var proxyConfigProvider = serviceProvider.GetRequiredService<IProxyConfigProvider>();
                    var yarpConfig = proxyConfigProvider.GetConfig();

                    var routedClusters = yarpConfig.Clusters
                        .SelectMany(t => t.Destinations!,
                            (clusterId, destination) => new { clusterId.ClusterId, destination.Value });

                    var groupedClusters = routedClusters.ToList();

                    foreach (var clusterGroup in groupedClusters)
                    {
                        var routeConfig = yarpConfig.Routes.FirstOrDefault(q => q.ClusterId == clusterGroup.ClusterId);

                        var url = clusterGroup.Value.Address;
                        options.SwaggerEndpoint($"{url}/swagger/v1/swagger.json",
                            $"{routeConfig!.RouteId.Replace("-", string.Empty)} API");
                    }
                });
            }

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapReverseProxy();
            });
        }
    }
}
